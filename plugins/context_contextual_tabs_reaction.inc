<?php 

/**
 * Set a menu local task using a context reaction.
 */
class contextContextualTabsReaction extends context_reaction {

  /**
   * Options form.
   */
  function options_form($context) {
    $defaults = $this->fetch_from_context($context, 'options');

    $form = array();
    $form['path'] = array(
      '#type' => 'textfield',
      '#title' => t('Path'),
      '#default_value' => isset($defaults['path']) ? $defaults['path'] : '',
    );

    if (module_exists('token')) {
      $form['tokens']['tokens'] = array(
        '#markup' => theme('token_tree_link', array('token_types' => array('user', 'node'))),
      );
    }
    return $form;
  }

  /**
   * Execute.
   */
  function execute(&$data, $root_path) {
    foreach ($this->get_contexts() as $context) {
      $options = $this->fetch_from_context($context, 'options');

      $token_data = array(
        'user' => menu_get_object('user'),
        'node' => menu_get_object('node')
      );
      $options['path'] = token_replace($options['path'], $token_data, array('clear' => TRUE));
      
      // Load the menu_local_tasks by the given path.
      $tasks = context_contextual_tabs_menu_local_tasks($options['path']);

      // Readd current tabs at the next available level.
      foreach ($data['tabs'] as $delta => $tabs) {
        $tasks['tabs'][] = $tabs;
      }

      $data['tabs'] = $tasks['tabs'];      
    }
  }
}
